package main

import (
	"log"
	"net/http"
	"time"
)

const (
	urlToCheck  = "http://doer.today"
	elapsedTime = time.Minute * 5
)

func HealthChecker(url string, checkFrequency time.Duration) {
	ticker := time.Tick(checkFrequency)
	for _ = range ticker {
		res, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		if res.StatusCode != 200 {
			log.Printf("Could not conncect to page %s status code %d", url, res.StatusCode)
		}
		log.Printf("Status is %d for url %s", res.StatusCode, url)
	}
}

func main() {
	HealthChecker(urlToCheck, elapsedTime)
}
